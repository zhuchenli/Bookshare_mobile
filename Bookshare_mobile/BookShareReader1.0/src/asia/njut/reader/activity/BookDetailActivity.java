package asia.njut.reader.activity;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import asia.njut.reader.R;
import asia.njut.reader.R.layout;
import asia.njut.reader.R.menu;

public class BookDetailActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_book_detail);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.book_detail, menu);
		return true;
	}

}
