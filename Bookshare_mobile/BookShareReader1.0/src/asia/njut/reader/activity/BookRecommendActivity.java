package asia.njut.reader.activity;

import java.util.List;
import java.util.Locale;
import java.util.Map;


import asia.njut.reader.view.PullRefreshListView;
import asia.njut.reader.view.PullRefreshListView.OnRefreshListener;


import asia.njut.reader.model.DataIniter;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import asia.njut.reader.R;
import asia.njut.reader.R.id;
import asia.njut.reader.R.layout;
import asia.njut.reader.R.menu;
import asia.njut.reader.R.string;

public class BookRecommendActivity extends FragmentActivity  implements OnRefreshListener  {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;
	private PullRefreshListView mRefreshListView;
	private List<Map<String, Object>> mDataSource;
	private listViewAdapter mAdapter;
	private int pageSize = 10;
	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_book_recommend);

		ActionBar bar = this.getActionBar();
		bar.setDisplayHomeAsUpEnabled(true);
		bar.setDisplayShowTitleEnabled(true);
		bar.setTitle(R.string.book_list);
		
		mRefreshListView = (PullRefreshListView) findViewById(R.id.main_listview);
		mDataSource = DataIniter.initValue(1, 15);
		mAdapter = new listViewAdapter();
		mRefreshListView.setAdapter(mAdapter);
		mRefreshListView.setonRefreshListener(this);
		
		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.book_recommend, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return false;
		}
	}
	
	
	private class listViewAdapter extends BaseAdapter {
		int count = mDataSource.size();

		@Override
		public int getCount() {
			return count;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = LayoutInflater.from(BookRecommendActivity.this).inflate(
					R.layout.booklistview_item, null);
			TextView title = (TextView) view.findViewById(R.id.item_title);
			TextView text = (TextView) view.findViewById(R.id.item_subtext);
			title.setText(mDataSource.get(position).get("title").toString());
			text.setText(mDataSource.get(position).get("subtext").toString());
			return view;
		}

	}

	private void chageListView(int pageStart, int pageSize) {
		List<Map<String, Object>> data = DataIniter.initValue(pageStart,
				pageSize);
		for (Map<String, Object> map : data) {
			this.mDataSource.add(map);
		}
		data = null;
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			mAdapter.count += pageSize;
			mAdapter.notifyDataSetChanged();
			mRefreshListView.onRefreshComplete();
			super.handleMessage(msg);
		}
	};

	@Override
	public void onPullUpRefresh() {
		new Thread() {
			public void run() {
				try {
					//模拟网络请求时间
					Thread.sleep(3 * 1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				chageListView(mDataSource.size() + 1, pageSize);
				handler.sendEmptyMessage(0);
			}
		}.start();
	}

	@Override
	public void onPullDownRefresh() {
		new Thread() {
			public void run() {
				try {
					Thread.sleep(3 * 1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				chageListView(mDataSource.size() + 1, pageSize);
				handler.sendEmptyMessage(0);
			}
		}.start();
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			Fragment fragment = new DummySectionFragment();
			Bundle args = new Bundle();
			args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position + 1);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			}
			return null;
		}
	}

	/**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	
	public static class DummySectionFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */

		
	
		
		
		public static final String ARG_SECTION_NUMBER = "section_number";

		public DummySectionFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			
					
			
			View rootView = inflater.inflate(
					R.layout.fragment_book_recommend_dummy, container, false);
			
			TextView dummyTextView = (TextView) rootView
					.findViewById(R.id.section_label);
			switch (getArguments().getInt(
					ARG_SECTION_NUMBER)) {
			case 1:
				dummyTextView.setText("图书推荐");
				
				
				
			case 2:
				dummyTextView.setText("热门图书");
			case 3:
				dummyTextView.setText("所有图书");
			}
			
	
			return rootView;
	
	
	}
		
	}	

}
