package asia.njut.reader.activity;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import asia.njut.reader.R;
/**
 * 
 * @ClassName: WelcomeActivity
 * @Description:启动全屏Activity
 * @author ZTS
 * @date 2013-9-7 下午1:21:15
 * 
 */
public class WelcomeActivity  extends Activity{
	   //延迟3.0秒 
		private final long DISPLAY_DURATION = 3000; 
		
		@Override
	    public void onCreate(Bundle savedInstanceState) {
			//savedInstanceState=null;
	        super.onCreate(savedInstanceState);
	        //取消标题栏
	        requestWindowFeature(Window.FEATURE_NO_TITLE);
	        //全屏
	        getWindow().setFlags(WindowManager.LayoutParams. FLAG_FULLSCREEN ,
	                      WindowManager.LayoutParams. FLAG_FULLSCREEN);
	        setContentView(R.layout.book_welcome);	
		
	        new Handler().postDelayed(new Runnable(){   
	        	String PREFS_NAME = "asia.njut.book";
				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);    
	        	
	            public void run() {  
	            	if (settings.getString("IsLogined", "NO").equals("YES")) {
	            		Toast.makeText(getApplicationContext(), "欢迎回来",
	   					     Toast.LENGTH_SHORT).show();
	            		  Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
			                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			                WelcomeActivity.this.startActivity(intent);  
					}else {
						Toast.makeText(getApplicationContext(), "您尚未登陆",
							     Toast.LENGTH_SHORT).show();
						Intent intent = new Intent(WelcomeActivity.this, LoginActivity.class);
		                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		                WelcomeActivity.this.startActivity(intent);  
					}
	            	
		                WelcomeActivity.this.overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
		                WelcomeActivity.this.finish();   
	            }           
	        }, DISPLAY_DURATION);   
	       
	    }
		
		
		
		
		@Override
		protected void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
		}




		@Override
		protected void onDestroy() {
			// TODO Auto-generated method stub
			super.onDestroy();
		}
	
}
